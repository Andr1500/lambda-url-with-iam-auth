#!/bin/sh

# Request parameters
METHOD="POST"
SERVICE="lambda"
REQUEST_PAYLOAD='{"inputText": "Request to Lambda."}'  # simple payload

# Environment variables
AWS_ACCESS_KEY=$AWS_ACCESS_KEY_ID
AWS_SECRET_KEY=$AWS_SECRET_ACCESS_KEY
REGION=$AWS_REGION
HOST=$LAMBDA_FUNCTION_HOST
ENDPOINT="https://${HOST}/"

# Check if environment variables are set
if [ -z "$AWS_ACCESS_KEY" ] || [ -z "$AWS_SECRET_KEY" ] || [ -z "$HOST" ]; then
  echo "Error: AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and LAMBDA_FUNCTION_HOST must be set as environment variables."
  exit 1
fi

# Get the current date and time
AMZ_DATE=$(date -u +"%Y%m%dT%H%M%SZ")
DATE_STAMP=$(date -u +"%Y%m%d")

# Create a canonical request
CANONICAL_URI="/"
CANONICAL_QUERYSTRING=""
CANONICAL_HEADERS="content-type:application/json\nhost:${HOST}\nx-amz-date:${AMZ_DATE}\n"
SIGNED_HEADERS="content-type;host;x-amz-date"
PAYLOAD_HASH=$(printf "$REQUEST_PAYLOAD" | openssl dgst -sha256 | sed 's/^.* //')
CANONICAL_REQUEST="${METHOD}\n${CANONICAL_URI}\n${CANONICAL_QUERYSTRING}\n${CANONICAL_HEADERS}\n${SIGNED_HEADERS}\n${PAYLOAD_HASH}"

# Create a string to sign
ALGORITHM="AWS4-HMAC-SHA256"
CREDENTIAL_SCOPE="${DATE_STAMP}/${REGION}/${SERVICE}/aws4_request"
STRING_TO_SIGN="${ALGORITHM}\n${AMZ_DATE}\n${CREDENTIAL_SCOPE}\n$(printf "$CANONICAL_REQUEST" | openssl dgst -sha256 | sed 's/^.* //')"

# Create the signing key
kSecret=$(printf "AWS4${AWS_SECRET_KEY}" | xxd -p -c 256)
kDate=$(printf "${DATE_STAMP}" | openssl dgst -sha256 -mac HMAC -macopt hexkey:"$kSecret" | sed 's/^.* //')
kRegion=$(printf "${REGION}" | openssl dgst -sha256 -mac HMAC -macopt hexkey:"$kDate" | sed 's/^.* //')
kService=$(printf "${SERVICE}" | openssl dgst -sha256 -mac HMAC -macopt hexkey:"$kRegion" | sed 's/^.* //')
kSigning=$(printf "aws4_request" | openssl dgst -sha256 -mac HMAC -macopt hexkey:"$kService" | sed 's/^.* //')

# Create the signature
SIGNATURE=$(printf "$STRING_TO_SIGN" | openssl dgst -sha256 -mac HMAC -macopt hexkey:"$kSigning" | sed 's/^.* //')

# Create the authorization header
AUTHORIZATION_HEADER="${ALGORITHM} Credential=${AWS_ACCESS_KEY}/${CREDENTIAL_SCOPE}, SignedHeaders=${SIGNED_HEADERS}, Signature=${SIGNATURE}"

# Output the necessary information for test tool like Postman
echo  "Endpoint: ${ENDPOINT}\n"
echo  "x-amz-date: ${AMZ_DATE}\n"
echo  "Authorization: ${AUTHORIZATION_HEADER}\n"
echo  "Payload: ${REQUEST_PAYLOAD}\n"

# Output the curl command
echo  "curl -X ${METHOD} \"${ENDPOINT}\" -H \"Content-Type: application/json\" -H \"x-amz-date: ${AMZ_DATE}\" -H \"Authorization: ${AUTHORIZATION_HEADER}\" -d '${REQUEST_PAYLOAD}'\n"