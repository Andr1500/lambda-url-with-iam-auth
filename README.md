#  AWS Lambda URL Invocations with IAM Authentication and Throttling Limits

My post on Medium about the project:

[AWS Lambda URL Invocations with IAM Authentication and Throttling Limits](https://medium.com/p/27dca8cf857e)

## Prerequisites

Before you start, make sure the following requirements are met:
- An AWS account with permissions to create resources.
- [AWS CLI](https://aws.amazon.com/cli/) installed on your local machine.

## Deployment

1. **Clone the repository.**

2. **Check and increase the Lambda function Concurrent executions quota (optional step).**

```
aws service-quotas get-service-quota \
    --service-code lambda \
    --quota-code L-B99A9384

aws service-quotas request-service-quota-increase \
    --service-code lambda \
    --quota-code L-B99A9384 \
    --desired-value 100
```

3. **Fill in all necessary parameters in the infrastructure/root.yaml CloudFormation template and create the CloudFormation stack.**

```
aws cloudformation create-stack \
    --stack-name invoke-lambda-url-iam-auth \
    --template-body file://infrastructure/root.yaml \
    --capabilities CAPABILITY_NAMED_IAM \
    --disable-rollback
```

4. **Retrieve outputs for setting up environment variables.**

```
aws cloudformation describe-stacks \
    --stack-name invoke-lambda-url-iam-auth \
    --query "Stacks[0].Outputs" --output json

```

5. **Set up region, Lambda function host, Access, and Secret keys for the created IAM user as environment variables in the test environment.**

```
export AWS_ACCESS_KEY_ID=<Access Key>
export AWS_SECRET_ACCESS_KEY=<Secret Access Key>
export AWS_REGION=<Region>
export LAMBDA_FUNCTION_HOST=<function-url.lambda-url-region.amazonaws.com>
```

6. **Generate AWS Signature v4 and POST request using the generate_sigv4.sh script.**

```
./generate_sigv4.sh

curl -X POST "https://.<url-id>.<region>.on.aws/" -H "Content-Type: application/json" -H "x-amz-date: 20240619T150022Z" -H "Authorization: AWS4-HMAC-SHA256 Credential=<ACCESS-KEY>/20240619/<region>/lambda/aws4_request, SignedHeaders=content-type;host;x-amz-date, Signature=<signarure-content>" -d '{"inputText": "Request to Lambda."}'
```

7. **Delete  the CloudFormation stack.**

```
aws cloudformation delete-stack --stack-name invoke-lambda-url-iam-auth
```

## Infrastructure schema and test results

*Infrastructure schema*

![schema](images/lambda_url_iam.png)

*Generate signature and request to the Lambda function URL from CLI*

![test_cli](images/request_from_cli.png)

*Request to the Lambda function from Postman*

![postman_correct_response](images/request_correct_response.png)

![postman_expired_sig](images/request_expired_sig.png)


You can support me with a virtual coffee https://www.buymeacoffee.com/andrworld1500 .